# Architecture
- - -

## Overview

Translate this
[Image of old view controller]

to this
[Image of viper stack]

### Guiding Principles
- Single Responsibility
- Opt for testability
- Composition over inheritance

### Base Classes
Provide the underlying implementation for VIPER stack initialization to encapsulate boilerplate and reduce memory graph errors.
Each of the following classes have Base implementations and should be subclassed. See [history](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/README-Architecture-History.md) for more context.

Coordinator - Wireframe - Interactor - Presenter - View Controller

#### Wireframe
###### _Description_
- Encapsulate logic to wire and build a VIPER stack
- Essentially a stack factory with make() function

###### _Guidelines_
- Use this class to return variants based on context e.g. `GCOPresenter`.

###### _Implementation_
```swift
class [Sample]Wireframe: BaseWireframe {

    /// Inject data here if needed by interactor
    public init(appDependencies: AppDependencies) {
        super.init(appDependencies: appDependencies)
    }

    override public func buildInterface() -> PresentableViewInterface {
        let storyboard = UIStoryboard(name: "Sample", bundle: Bundle(for: type(of: self)))
        let viewController = storyboard.instantiateViewController(withIdentifier: "[Sample]ViewController") as! [Sample]ViewController
        return viewController
    }

    override public func buildInteractor() -> [Sample]Interactor {
        return [Sample]Interactor(appDependencies: appDependencies)
    }

    override public func buildPresenter() -> [Sample]Presenter {
        return [Sample]Presenter(input: interactor)
    }
}
```

#### Interactor
###### _Description_
- Encapsulate business logic for a particular screen.
- Inject and use `AppDependencies` struct to fetch data via services and monitor state changes via observers.

###### _Guidelines_
 - Inject `AppDependencies` via `init()`
 - Specific focus on writing highly testable code.
 - Do not include any view references.
 - Should not need to import `UIKit`.

###### _Implementation_
```swift
public protocol [Sample]InteractorInput {
	func loadData()
}
```
* Defines what business logic this interactor can perform
* Presenter calls these methods

```swift
public protocol [Sample]InteractorOutput {
	func didGetCards(cards: [StarbucksCardRepresentable])
}
```
* Defines the results for a given input
* Presenter receives output messages.

```swift
public class [Sample]Interactor: BaseInteractor {

   /// Output is the presenter
	var output: [Sample]InteractorOutput? {
		return _output as? [Sample]InteractorOutput
	}

	/// Router is the coordinator
	var router: [Sample]Router {
		return _router as! [Sample]Router
	}

	/// Inject data into the init
	init(appDependencies: AppDependencies){
		super.init(appDependencies: appDependencies)
	}
}
```
* Sample interactor subclass

```swift
extension [Sample]Interactor: [Sample]InteractorInput {
	func loadData() {
		/// ...
		/// perform fetch
		/// ...
		output?.didGetCards(cards: cards)
	}
}
```
* Provides the implementation for interactor methods


#### Coordinator
###### _Description_
###### _Guidelines_
###### _Implementation_

#### Interactor
###### _Description_
###### _Guidelines_
###### _Implementation_

#### Presenter
###### _Description_
###### _Guidelines_
###### _Implementation_

#### View Controller
###### _Description_
###### _Guidelines_
###### _Implementation_

---

#### © 2018 Starbucks · [Home](https://scm.starbucks.com/gdp-ios/starbucks-ios) · [Documentation](https://scm.starbucks.com/pages/gdp-ios/docs/) 
