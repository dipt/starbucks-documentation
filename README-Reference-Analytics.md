# Analytics
---

## Overview
- We send up analytics information when a user views or takes action on events in our application.
- Sometimes you'll hear it being called UO or Universal Object. It is a thin layer so that we don't have to concern ourselves with the underlying implementation (mostly Google Tag).

- The two main facilities you will concern yourself with are:
	- **`SBXAnalyticsManager.h/m`**:
		- Singleton and our main resource for sending analytics
   - **`AnalyticsService.swift`**:
- Protocol for analytic sending. Currently `SBXAnalyticsManagerWrapper` conforms. It just wraps `SBXAnalyticsManager`.
- We needed a facility to use the manager outside of the app target (ex. in `SBXFeatureKit`).
- The two methods you will use are `trackScreen` and `trackEvent`:
	- `+ (void)trackEvent:(nonnull NSString*)category action:(nonnull NSString*)action forFeature:(nullable NSString*)feature;`
	- `+ (void)trackEvent:(nonnull NSString*)category action:(nonnull NSString*)action withDimensions:(nullable NSDictionary<NSString*, id>*)dimensions forFeature:(nullable NSString*)feature;`


## Specifications
### Description
The specs are a mess. Been working with the TPMs to provide us better ones. By the time you read this it may be updated. The current ones have too much cruft so we'll try our best to get through it.

### Attributes
* **Screen / screenName**
	- This should be fired once every time a view appears. It's how analytics knows which screen you're on when you fire events.
* **Event Category**
   - Send this up for category and feature.
* **Event Action**
   - Taps, swipes, errors, sometimes even views for certain modals. Just copy and paste whats in the box and send it up with action.
* **Dimensions**
   - A dictionary of information for a certain object. Such as stores or gift card. Generally there should already be a facility somewhere that will build the dimension dictionary for you. One such example is in `AnalyticsDimensions.swift`. If there isn't then you'll need to build it yourself. To know what information to include you can check [this confluence document](https://starbucks-mobile.atlassian.net/wiki/spaces/UO/pages/137162116/Custom+Dimension+Categories#CustomDimensionCategories-card). It isn't updated very often so I would use the GitHub link in there as the source of truth.
   - Canonical is handled for you. It's prepackaged with every event call.
* **AdHoc Dimensions**
    - Isn't used as much but this is a single line dimension such as fieldDescription. That would be the key and usually somewhere in the doc they'll provide the value they want for it. You can just attach this to any other dimension that gets packaged. We merge all of them together into one dictionary along with the canonical and send that up.

### Example
Here is example 1 from our Stores 5.0 refresh:

![AnalyticsSpec1](resources/AnalyticsSpec1.png)

Example 2 from Gift 5.0 refresh:

![AnalyticsSpec2](resources/AnalyticsSpec2.png)
## How to
### Description
Lets use a simple Gift spec as an example. The /gift screen is the main Gift Browse view that you get when you first tap into Gift.

### Sample Data
| Screen         | Event Category | Action        | Dimensions    |
| -------------- | -------------- | ------------- | ------------- |
| /gift          | eGift          | Tap a card    | card          |
| /gift          | eGift          | View a card   | card          |

### Screen View Event
In our `GiftBrowseViewController`'s `viewWillAppear` we'll want to add in the track screen call.

```Swift
override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    SBXAnalyticsManager.trackScreen("/gift", withDimensions: nil, forFeature: "eGift")
}
```

### Tap a card Event
This event should fire every time a user taps a card. In our UITableViewDelegate's didSelectRow we'll want to add a track event call.

```swift
public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let card = dataSource[indexPath.row]
    let dimensions = AnalyticDimensions.eGift(cardNumber: card.number, categoryNumber: card.categoryNumber)
    SBXAnalyticsManager.trackEvent("eGift", action: "Tap a card", withDimensions: dimensions, forFeature: "eGift")
}
```

### View a Card Event
This event should fire every time a user "views" a card. In our UITableViewDataSource's cellForRow we'll want to add a track event call.

```swift
public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let card = dataSource[indexPath.row]
     let dimensions = AnalyticDimensions.eGift(cardNumber: card.number, categoryNumber: card.categoryNumber)
     SBXAnalyticsManager.trackEvent("eGift", action: "View a card", withDimensions: dimensions, forFeature: "eGift")
     let cell = GiftCardCell()
     return cell
}
```
---

#### © 2018 Starbucks · [Home](https://scm.starbucks.com/gdp-ios/starbucks-ios) · [Documentation](https://scm.starbucks.com/pages/gdp-ios/docs/) 

