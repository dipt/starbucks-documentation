# API & Environments
---

### Orchestra


### Open API

###### Environments
* **Test 19** - This is the first environment where new API endpoints are spun up.
* **Test 14** - Use this environment for newer API endpoints that are not in TestMain yet.
* **Test Main** - This is the environment the majority of development work will be done on.
* **Production** - Live ammo
 
###### Migration
Migration from test environments happens 1x a week, usually on Thursday or Friday.
Each API change will first hit Test 19 which is a CI/CD environment.
After a week of testing in Test 19, the API change will be migrated to Test 14.
And so on, up until Prod.

Be aware if developing a new feature that will rely on a new API endpoint,
that this cycle will take a few weeks before API changes are reflected in TestMain/Prod environs.

###### Useful Links
* [API Home](https://starbucks-mobile.atlassian.net/wiki/spaces/AH/overview) - API Confluence space
* [API Environments](https://starbucks-mobile.atlassian.net/wiki/spaces/AH/pages/33948399/Environments) - List of environments and purpose
* [Stores](https://starbucks-mobile.atlassian.net/wiki/spaces/AH/pages/51773461/Stores) - Stores available for testing for each environment
* [Endpoints](http://digitalarchitecture.starbucks.net/index.php?title=OPEN_API) - Endpoints


---

#### © 2018 Starbucks · [Home](https://scm.starbucks.com/gdp-ios/starbucks-ios) · [Documentation](https://scm.starbucks.com/pages/gdp-ios/docs/) 
