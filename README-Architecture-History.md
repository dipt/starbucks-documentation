# History
### Architecture for Starbucks iOS Application
---

Provides historical reference for the evolution of our code base. Any new paradigms and patterns should be added to this document. Note this document is in reverse chronological order.

## Introduction of Observer


## Introduction of Worker

  
## Introduction of Coordinator Subclasses
### Problem Statement
 - Numerous custom navigation paradigms:
   - Bottom sheet container
   - Navigation container with custom app bar

### Solution
- Created sub class variants for each navigation structure

### Implementation
- Pay 5.0

## Introduction of Coordinator and Wireframe
### Problem Statement
 - Lot of coordinator boilerplate
 - Cumbersome flow for routing/ presenting new screen, P -> I -> P -> W
 
### Solution
- Created base class for coordinator
- Moved routing from presenter to wireframe to interactor to coordinator

### Implementation
- Pay 5.0

## Introduction of VIPER Base Classes
### Problem Statement
 - Numerous classes to create and wire up
 - Manage strong/ weak references, complicated memory graph
 - Lot of boilerplate
 - Inconsistent naming
 
### Solution
 - Created base classes to encapsulate boilerplate stack wiring and set up
 - Encapsulates reference graph
 
### Implementation
- Pay 5.0

## Introduction of Coordinators
### Problem Statement
 - Need to encapsulate the how to navigate to other screens

### Solution
 - **Coordinator** introduced to encapsulate navigation logic and coordinate between between VIPER stacks.
 
### Implementation
- Stores 5.0

## Introduction of App Dependencies
### Problem Statement
 - Needed a host for newly created Stores service
 - Promote dependency injection over singleton
  
### Solution
- Created a struct **AppDependencies**

### Implementation
- Stores 5.0

## Introduction of Services
### Problem Statement
 - Stores 5.0 needed to hit orchestra endpoints
 - Stores 5.0 needed to be able to retrieve data from
 - Stores 5.0 faced with changing scheme and unavailable
 
### Solution
 - Created a Service abstraction protocol for the stores endpoints
 - Mocks and concrete implementations created conforming to service definition
 - Provides a consistent API for downstream components (Interactor)

### Implementation
- Stores 5.0

## Introduction of VIPER
### Problem Statement
 - Multiple implementations of similar business logic
 - Inconsistent instantiation of view controller initialization
 - Vary names for different objects, not a clear definition of what each does. (Helpers, Managers, Datasource)
 - Unclear data flow, source of truth, local caching and persistence.
 - Navigation coupling
 - Poor maintainability, extensibility
 - Hard to write unit tests
 
### Solution
 - Separate mvc to single-responsibility classes
 - Define clear responsibilities
 - Presentation Slides
 
### Implementation
- Stores 5.0

---

#### © 2018 Starbucks · [Home](https://scm.starbucks.com/gdp-ios/starbucks-ios) · [Documentation](https://scm.starbucks.com/pages/gdp-ios/docs/) 

