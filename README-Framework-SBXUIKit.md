# SBXUIKit
---

Starbucks 5.0 component library. 

* [Component JIRA Epic](https://starbucks-mobile.atlassian.net/browse/IG-1194)
* [Component Zeplin Project](https://zpl.io/bzY0LMA)

### Elements
* [Color](https://scm.starbucks.com/pages/gdp-ios/docs/Frameworks/SBXUIKit/docs/Structs/SBXUIColor.html)
* Shadows
* Typography

### Components
* Button
	* Fill 
	* Ghost
	* FRAP 
* Textfield

### Structures
* App Bar - Design - Custom app bar. (Created before Apple's iOS 11 nav bar.)
* App Bar Container - Wraps custom app bar into a convenient package.
* Alerts
* Bottom Sheet
* Order Toolbar

### Demo App
Scheme: `SBXUIKitDemo`

A lightweight app target that includes `SBXUIKit` to showcase and test a new UI components before integration.

### Accessibility
An accessiblity specification is created for each UI element and should be implemented within the base definition for consistency. See [Component Zeplin Project](https://zpl.io/bzY0LMA) for respective component specification. Refer to [Accessibility Guide](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-Reference-Accessibility.md) for latest practices and data structures.

### Dynamic Type
Currently unsupported. See [Jira Ticket]() for latest development.

---

#### © 2018 Starbucks · [Home](https://scm.starbucks.com/gdp-ios/starbucks-ios) · [Documentation](https://scm.starbucks.com/pages/gdp-ios/docs/) 
