# New Relic
---

This SDK supports our crash reporting and analytics.

## Analytics Events

## Crash Reporting

## Exception Handling

###### Resources
* [Starbucks Analytics Guide](README-Reference-Analytics.md) - How to read starbucks analytics specification.

### Getting Started

### Testing

### Developer Notes

---

#### © 2018 Starbucks · [Home](https://scm.starbucks.com/gdp-ios/starbucks-ios) · [Documentation](https://scm.starbucks.com/pages/gdp-ios/docs/) 
