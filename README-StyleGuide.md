# Style Guide

### Standardize nomenclature and convention methodology
---

## Swift Guidlines
[Complete Reference](https://swift.org/documentation/api-design-guidelines/)

## Nomenclature
### Basic
- Filename should represent the primary structure / type / class in the file's contents
- Do **not** use file prefix {SBA}.

### New File
- If a struct, class, extension, protocol, or other `public` component is present in a file with other types, it should be moved to its own file. Basically anything not `fileprivate`

### Reserved Architecture Names
- Coordinator
- Interactor
- Presenter
- Worker
- Observer

```Swift
TransferBalanceWorker
PayRootCoordinator
```

### Reserved Pattern Names
- Observable

```Swift 
Source(<Status>)
Observable(<Status>)
```

## Methods


## Protocols
- Use verb
 
```Swift
UserRepresentable
AlertPresentable
```

## Extensions
- Convention: {Class_Name}+{Framework}.swift
- Keep public extensions together.
 
```Swift
UITableView+SBXUIFeatureKit.swift
UICollectionView+SBXUIKit.swift
```

--- 
#### © 2018 Starbucks · [Home](https://scm.starbucks.com/gdp-ios/starbucks-ios) · [Documentation](https://scm.starbucks.com/pages/gdp-ios/docs/) 

