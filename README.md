# Starbucks iOS Application
---
This repository hosts the production iOS application for US, GB and Canada.  

## Style Guide

Standardize nomenclature methodology.

* [**Starbucks Style Guide**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-StyleGuide.md) - Starbucks defined conventions and nomenclature
* [**Swift Style Guide**](https://swift.org/documentation/api-design-guidelines/) - Style guide reference

## Build Configuration & Environments

Defines the environments, scheme, build configuration and build distribution. 

* [**Build Configuration**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-Configuration.md) - Schemes, configuration and distribution methods
* [**Environments**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-Configuration-Environments.md) - Internal APIs and environments for OpenAPI and Orchestra


## Architecture

Provides in-depth discussion of our architecture and design patterns used throughout the application. 

### Overview

* [**Overview**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-Architecture.md) - Provides an overview of the latest architecture patterns and conventions
* [**History**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-Architecture-History.md) - Provides a timeline for the architecture evolution in reverse chronological order

### Patterns

Captures design patterns used throughout the application. Refer to the following links for detail discussion on intended use cases, implementation and road map.

* [**Observable**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-Pattern-Observable.md) - Broadcast, one-to-many pattern

### Frameworks

* [**SBXCommons**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-Framework-SBXCommons.md) - Foundation extensions
* [**SBXCoding**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-Framework-SBXCoding.md) - Encryption helpers
* [**SBXUIKit**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-Framework-SBXUIKit.md) - Commonly used UI elements and structures
* [**SBXFeatureKit**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-Pattern-SBXFeatureKit.md) - Encapsulate feature sets

## Testing

Defines strategy and best practices to write tests code.

* [**Testing**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-Testing.md) - Overall strategy and methodology
* [**UI Automation**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-Testing-UIAutomation.md) - Overview and SDET managed resources

## Development Guides

These guides capture best practices and useful hints for each domain.

* [**Deep Linking Guide**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-Reference-DeepLinking.md) - Deep linking design and implementation
* [**Analytics Guide**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-Reference-Analytics.md) - Logging events and decoding the analytics specification
* [**Localization Guide**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-Reference-Localization.md) - Localizing content and string import script
* [**FB Tweaks Guide**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-Reference-FBTweaks.md) - Development toggles, feature flags and mocking 
* [**Accessibility Guide**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-Reference-Accessibility.md) - Best practices and helpful tips

## Third Party

Encapsulates integration, usage, business logic for each of our third party libraries. 

* [**New Relic**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-ThirdParty-NewRelic.md) - Analytics 
* [**AppSee**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-ThirdParty-AppSee.md) - Analytics
* [**Apptentive**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-ThirdParty-Apptentive.md) - Reviews and customer support
* [**Apptimize**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-ThirdParty-Apptimize.md) - A/B Testing
* [**Branch**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-ThirdParty-Branch.md) - Notifications, marketing and deep linking
* [**Urban Airship**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-ThirdParty-UrbanAirship.md) - Push notifications and marketing

## FAQ
Starbucks specific knowledge to provide context and historical insight into design decisions. 

* [**Overview**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-Domain.md) - Starbucks specific domain knowledge, acronyms and business requirements.

## Documentation
Docs' docs.

* [**Documentation Guide**](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-Documentation.md) - `man man`

--- 
#### © 2018 Starbucks · [Home](https://scm.starbucks.com/gdp-ios/starbucks-ios) · [Documentation](https://scm.starbucks.com/pages/gdp-ios/docs/) 
