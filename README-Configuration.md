# Build Configuration, Distribution & Schemes
---

## Configuration
* Debug
* Enterprise
* Release

## Distribution
* [Hockey](https://rink.hockeyapp.net/users/sign_in)
* [Jenkins](https://mobilejenkins.starbucks.net/)
	* [Develop Pipeline](https://mobilejenkins.starbucks.net/job/us-ios-develop-pipeline/)
	* [Release Pipeline](https://mobilejenkins.starbucks.net/job/us-ios-release/)
	* [Self Service](https://mobilejenkins.starbucks.net/job/us-ios-self-service/)

## Schemes

* **Starbucks TestMain**
	* Use for testing
* **Starbucks Production**
	* **Live Ammo**


---

#### © 2018 Starbucks · [Home](https://scm.starbucks.com/gdp-ios/starbucks-ios) · [Documentation](https://scm.starbucks.com/pages/gdp-ios/docs/) 
