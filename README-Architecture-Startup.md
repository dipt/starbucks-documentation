# Start-Up
---

## Overview

Overview of our start-up process until root view controller is shown.

User perspective: 

```swift
Splash screen
Loading splash screen (embedded within AppRootViewController)
Root view (generally TabBarController(Home, Pay, Order, Stores, Gift)) 
```

Dev Perspective: 

```swift
main()
AppDelegate.didFinishLaunch()
AppDelegateRemote -> (DataSource, SDK1, SDK2...)
AppBootLoader(DataSource, ...) -> AppDependencies
AppRootCoordinator(AppDependencies) 
.showWindow()
``` 

## Implementation

### App Delegate

Entry point of our application is similar to all iOS apps. 

###### Guidelines

* Never reference `AppDelegate.shared`, find alternative solution.
* Leverage `AppBootRemote` to initialize code at start-up or interface with app delegate methods.
* Refrain from overloading app delegate methods directly. 
* For deep link support, see [DeepLink Guide](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-Reference-DeepLinking.md)


### App Delegate Remote (TBD)

###### Development

See [Documentation]() 

###### Guidelines
 
* For Third Party support, see [Home](https://scm.starbucks.com/gdp-ios/starbucks-ios) for relevant links.

### App Boot Loader

This class is to create and configure our `AppDependencies` which contains service and observer definitions.

###### Guidelines

* This is a good place to swap service definitions with mocked implementations.

### App Root Coordinator

This is the root of our application. It manages the window via `WindowCoordinator` and root VIPER stack `AppRoot*`.

See Documentation:

* [AppRootCoordinator]() - Hosts app window and root VIPER stack.
* [AppRootWireframe]() - Factory for stack, provides initializer for each class.
* [AppRootInteractor]() - Responsible for making the first calls to determine app state e.g. `AppConfig`, auth, GCO, and to determine the correct screen to show the user.
* [AppRootPresenter]() - Prepares data for presentation.
* [AppRootViewController]() - Due to legacy requirements, this view has an unorthodox view hierarchy; see **Legacy Implementation** below for details. This view controller embeds a loading view to show during loading or transition between the splash screen and root view controller. It also embeds, the tab bar controller containing all appropriate tabs based on app config, market, account state etc. Unseen by the user, the legacy view controller `SBAHomeViewController` is also embedded to host legacy implementation. 

## Legacy Implementation

#### Bottom Nav Transition

As part of the 5.0 UI uplift we transitioned from a custom header navigation to standard Apple `UITabBarController` navigation.

Pre 5.0 Home  |  5.0 Home
:-------------------------:|:-------------------------:
![Pre5.0](resources/BottomNav1.png)  |  ![5.0](resources/BottomNav2.png)

#### SBAHomeViewController

Before the UI update, `SBAHomeViewController` was the root of our application providing the container for the header navigation and dashboard feed. Since home was the root view and, therefore, always in memory it was handling more root functions such as wide notifications, displaying custom overlays, fetching data, and managing caches. Due to truncated timetable, we were not able to migrate all of home's funcationality to single-responsibility objects. However, we were able to devise a stratgy to migrate this functionality over time. First, we created a new `HomeRoot` stack which would own home and dashboard responsibilities. The remaining logic, a mix of start-up and state management code, was embedded into the new `AppRootViewController` view controller. This maintained it's relative place in the hierarchy for legacy implementation. (See **Note** below for migration notes.)

#### AppDelegate

An `AppRootCoordinator` was created to manage the window and the `AppRoot` stack which hosts the above mentioned `AppRootViewController`. To support this new root stack, we created a new swift, `AppDelegate`, to host the new root stack and created a reference to the original `SBAAppDelegate`. This provides a new foundation for our root coordinator stack and allows us the flexiblity to create a new formalized start-up mechanism and migrate functionality appropriately given developer and QA bandwidth. (See `AppDelegateRemote` for start-up mechanism details and migration help.)

Pre 5.0 App Delegate  |  5.0 App Delegate
:-------------------------:|:-------------------------:
![Pre5.0](resources/Pre5_0Root.png)  |  ![5.0](resources/5_0Root.png)

#### Migration Strategy

`SBAHomeViewController` and `SBAAppDelegate` are deprecated. All functions should be moved to appropriate places. Suggestions below:

* Start-Up Logic: Move to specialized `AppDelegateRemote` class
* App State Logic: Move to `AppRootInteractor` or observer.
* Home Logic: Move to `HomeRoot` stack
* Dashboard Logic: Move to `Dashboard` stack
* Overlay/ Onboarding Logic: Create stack for screen(s), observe state within the `AppRootInteractor` and present via `AppRootCoordinator`

---

#### © 2018 Starbucks · [Home](https://scm.starbucks.com/gdp-ios/starbucks-ios) · [Documentation](https://scm.starbucks.com/pages/gdp-ios/docs/) 

