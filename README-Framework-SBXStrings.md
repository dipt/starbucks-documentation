# SBXStrings
### Starbucks Framework
---

### Overview

### Localization

For best practices and string import steps. See [Localization Guide](https://scm.starbucks.com/gdp-ios/starbucks-ios/blob/develop/Documentation/README-Reference-Localization.md). 


---

#### © 2018 Starbucks · [Home](https://scm.starbucks.com/gdp-ios/starbucks-ios) · [Documentation](https://scm.starbucks.com/pages/gdp-ios/docs/) 
