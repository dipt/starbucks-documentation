# Documentation
---

This is the doc for the docs. Please follow instructions and guidelines to keep documents up to date.

### Generating Docs
* Find  `GenerateDoc.sh` in `/Documentation`
* Run script `./GenerateDoc.sh`

### Guidelines
* Run script and merge updated docs, suggest, weekly. 

### Known Issues
* Docs are generated for swift code only. Objc is supported, just needs to be configured.
* Jazzy error `A compile error prevented [..] from receiving a unique USR. Documentation may be incomplete. Please check for compile errors by running [..]`
* Root index.html is manually generated. Frameworks are individually added. Create a script to link frameworks and ingest main README. 
* SBXStrings is not generating docs.

--- 
 
#### © 2018 Starbucks · [Home](https://scm.starbucks.com/gdp-ios/starbucks-ios) · [Documentation](https://scm.starbucks.com/pages/gdp-ios/docs/) 
