# FAQ
---

Starbucks specific domain knowledge, acronyms and business requirements.

### Useful Confluence Links

iOS Developer Confluence Space (UBM)

* [UBM Home](https://starbucks-mobile.atlassian.net/wiki/spaces/UBM/overview)
* [Architecture Meeting Notes and Action Items](https://starbucks-mobile.atlassian.net/wiki/spaces/UBM/pages/207159543/Architecture+Meeting+Notes)
* [Dev Meeting Notes and Action Items](https://starbucks-mobile.atlassian.net/wiki/spaces/UBM/pages/233832717/Dev+Meeting+Notes)
* [Interview Questions](https://starbucks-mobile.atlassian.net/wiki/spaces/UBM/pages/139839624/Interview+Questions)
* [WWDC Watch Party](https://starbucks-mobile.atlassian.net/wiki/spaces/UBM/pages/440139806/WWDC+Watch+Party)

### Acronyms
- SBX : Starbucks frameworks prefix (historically from the original shared business framework)
- SBA : The prefix for the main Starbucks App (target)
- SBF : Starbucks Feature Kit (modules that will be moved to SBXFeatureKit after Orchestra integration)
- GCO : Guest Checkout, allows users to mobile order without creating a traditional MRS account
- MOP : Mobile Order & Pay (originally XOP, some references still exist)
- SFE : Stars for Everyone (coming FY 2019)
- MTR : Multi-Tier Redemption (coming FY 2019)
- MSR : My Starbucks Rewards, the loyalty program
• MSR 2.0 (UK) - Earn a single star per transaction
• MSR 3.0 (US, CA) - Earn stars based on transaction amount
- UDP : Universal Digital Platform
- GTM : Google Tag Manager, analytics SDK driving UO
- UO : Universal Object, Starbucks' homegrown analytics paradigm
- UBM: Ugly Baby Makeover - iOS Dev group
- SSC: Starbucks Service Center

###### Useful Links
* [API Glossary](https://starbucks-mobile.atlassian.net/wiki/spaces/AH/pages/37421281/Glossary)


### SSC 

* [Conference Room Scheduling Assistant](https://partner.starbucks.com/SitePages/ConferenceRoomScheduling.aspx)
* [Maps](https://departments.starbucks.com/sites/corpfacilities/Pages/sscmaps.aspx)
* [Interactive Maps](http://wtfmi.surge.sh/)

--- 
 
#### © 2018 Starbucks · [Home](https://scm.starbucks.com/gdp-ios/starbucks-ios) · [Documentation](https://scm.starbucks.com/pages/gdp-ios/starbucks-ios/) 
