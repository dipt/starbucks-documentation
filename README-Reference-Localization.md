
# Localization
---

We support multiple languages within the application. All content is hosted and managed by our content team.  

### Content Source 

Types of strings:

* [**Master**](https://docs.google.com/spreadsheets/d/11WQ7ljLScujH-Uswk2aVfeh8Umv2M6z6xeQrK3trMkA/edit#gid=4) - User content 
* [**Accessibility**](https://docs.google.com/spreadsheets/d/11WQ7ljLScujH-Uswk2aVfeh8Umv2M6z6xeQrK3trMkA/edit#gid=14) - Accessibility strings, used during Voice Over
* [**API Errors**](https://docs.google.com/spreadsheets/d/11WQ7ljLScujH-Uswk2aVfeh8Umv2M6z6xeQrK3trMkA/edit#gid=853708768) - API errors strings

### Implementation

Strings in excel file get mapped to string constants which we use in the app. See **String Import** to update.


Localized String

```swift
let titleString = k7461.localizedString(withComment: "Add a phone number")
```
Localized String with variables

```swift
let rewardString = k8280.localizedString(withPlaceholders: ["~x~"], 
						   andReplacements: [125.0],
                            andComment: "That's ~x~ Stars for a free food or drink.")
```

### String Import

* Navigate to SBXString folder `cd {ProjectDir}/SBXStrings`
* Run script `./UpdateLocalizedStrings.sh`

> Note: Script should generate string constant files for each excel sheet, create and push branch and then open a webpage to prepare a PR. 




---

#### © 2018 Starbucks · [Home](https://scm.starbucks.com/gdp-ios/starbucks-ios) · [Documentation](https://scm.starbucks.com/pages/gdp-ios/docs/) 




